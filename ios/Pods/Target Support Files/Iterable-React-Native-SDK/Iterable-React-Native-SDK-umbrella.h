#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "RNIterable-Bridging-Header.h"

FOUNDATION_EXPORT double Iterable_React_Native_SDKVersionNumber;
FOUNDATION_EXPORT const unsigned char Iterable_React_Native_SDKVersionString[];

