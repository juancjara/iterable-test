//
//  NotificationService.h
//  NotificationExtension
//
//  Created by Juan Carlos Jara Loayza on 5/01/22.
//

#import <UserNotifications/UserNotifications.h>

@interface NotificationService : UNNotificationServiceExtension

@end
