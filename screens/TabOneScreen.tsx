import { StyleSheet } from "react-native";

import EditScreenInfo from "../components/EditScreenInfo";
import { Text, View } from "../components/Themed";
import { RootTabScreenProps } from "../types";
import {
  Iterable,
  IterableActionContext,
  IterableConfig,
} from "@iterable/react-native-sdk";
import { useEffect } from "react";

const API_KEY = "REPLACE_API_KEY";
const email = "jcjara@test.com";

const getToken = () => {
  return "REPLACE TOKEN";
};

export default function TabOneScreen({
  navigation,
}: RootTabScreenProps<"TabOne">) {
  useEffect(() => {
    const config = new IterableConfig();
    const token = getToken();

    config.authHandler = (...args) => {
      return new Promise((resolve, reject) => {
        resolve(token);
      });
    };

    config.autoPushRegistration = true;

    Iterable.initialize(API_KEY, config);
    setTimeout(() => {
      console.log("add email 2", email);
      Iterable.setEmail(email);
    }, 3000);
  }, []);
  return (
    <View style={styles.container}>
      <Text style={styles.title}>Tab One Iterable {email}</Text>
      <View
        style={styles.separator}
        lightColor="#eee"
        darkColor="rgba(255,255,255,0.1)"
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  title: {
    fontSize: 20,
    fontWeight: "bold",
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: "80%",
  },
});
